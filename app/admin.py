from django.contrib import admin

from app.models import MapDirection

admin.site.register(MapDirection)
