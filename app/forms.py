from django.forms import ModelForm

from app.models import MapDirection


class MapDirectionForm(ModelForm):
    class Meta:
        model = MapDirection
        fields = ['address']
