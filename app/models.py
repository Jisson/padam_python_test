from django.db import models


class MapDirection(models.Model):
    address = models.CharField("Address", max_length=255, blank=False)
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.address + ', ' + str(self.date)

