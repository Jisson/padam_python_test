from django.conf.urls import patterns, url

__author__ = 'pierre'


urlpatterns = patterns('app.views',
                       url(r'^$', 'direction_map', name='map'),
                       url(r'^submit$', 'direction_map_submit', name='direction-map-submit'),
                       )

