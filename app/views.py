# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import Http404, HttpResponse

import logging
import json

from app.forms import MapDirectionForm
from app.models import MapDirection


def direction_map(request):

    form = MapDirectionForm()

    return render(request, 'direction_map.html', {
        'form': form,
    })


def direction_map_submit(request):

    # print("submit_called")
    logging.debug('submit called')

    response_data = {}
    if request.method == 'POST' and request.is_ajax():
        form = MapDirectionForm(request.POST)
        if form.is_valid():
            map_direction = MapDirection()
            map_direction.address = form.cleaned_data.get('address')
            map_direction.save()
            logging.debug("AddressRequested=", map_direction.address)

            response_data['status'] = "success"
        else:
            response_data['status'] = "error"
            response_data['message'] = "Impossible d'obtenir votre itinéraire. Merci de vérifier l'adresse saisie."
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        raise Http404   # Raise HTTP because that method must be accessed through POST + Ajax

