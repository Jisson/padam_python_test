function calculateDirectionMap(address_from, address_to) {

    // Center initialized to Naples, Italy
    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(48.855901, 2.354765),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        unitSystem: google.maps.UnitSystem.METRIC
    };
    // Draw the map
    var mapObject = new google.maps.Map(document.getElementById("map"), mapOptions);

    var directionsService = new google.maps.DirectionsService();

    var directionsRequest = {
        origin: address_from,
        destination: address_to,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC
    };

    directionsService.route(
        directionsRequest,
        function(response, status)
        {
            if (status == google.maps.DirectionsStatus.OK)
            {
                new google.maps.DirectionsRenderer({
                    map: mapObject,
                    directions: response
                });
            }
            else
                $("#error").append("Impossible d'obtenir votre itin�raire. Merci de v�rifier l'adresse saisie.<br />");
        }
    );
}

$(document).ready(function() {

    // Verify if user browser support Geolocation API
    if (typeof navigator.geolocation == "undefined") {
        $("#error").text("Your browser doesn't support the Geolocation API");
        return;
    }

    function initialize(){
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(48.855901, 2.354765),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            unitSystem: google.maps.UnitSystem.METRIC,
        };
        // Draw the map
        var map = new google.maps.Map(mapCanvas, mapOptions)
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    function directionMapFormSubmit() {

        $.ajax({
            type: 'POST',
            url: '/map/submit',
            data: $("#map-direction-id").serialize(),
            cache:false,
            success: function (data, status) {

                if (data['status'] == "success") {

                    $("#error").text('');
                    var address_from = "37+rue+Dareau,Paris";
                    calculateDirectionMap(address_from, $("#id_address").val());

                } else if (data['status'] == "error") {
                    $("#error").text(data['message']);
                }
                else {
                    $("#error").text("Erreur inattendue... Merci de r�essayer plus tard.");
                }
            },
            error: function(data, status) {
                $("#error").text("Erreur inattendue... Merci de r�essayer plus tard.");
            }
        });
    }

    $("#map-direction-id").submit(function(event) {
        event.preventDefault();

        directionMapFormSubmit();
    });

});